<!DOCTYPE html>
<html>
  <head>
    <title>WebNots - Static Page Example</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>

    <!-- HEADER -->
    <div id="header" class="section header">
      <div class="menu-bar clearfix">
        <div class="logo">
        </div>
        <a href="#eventsSection" rel="" class="text" key="start"></a>
        <a href="#aboutUs" rel="" class="text" key="aboutUs"></a>
        <a href="#partners" rel="" class="text" key="partners"></a>
        <a href="#newsletter" rel="" class="text" key="newsletter"></a>
        <a href="#contact" rel="" class="text" key="contact"></a>
        <div id="mobile-menu" key="menu" class="mobile-menu text"></div>
        <select id="language">
          <option value="pl">PL</option>
          <option value="en">EN</option>
        </select>
      </div>
    </div>

    <!-- END OF HEADER -->

    <!-- MOBILE HEADER -->
    <div id="mobile-header" class="mobile-header">
      <div key="menu" class="close-menu text"></div>
      <a href="#eventsSection" rel="" class="text" key="start"></a>
      <a href="#aboutUs" rel="" class="text" key="aboutUs"></a>
      <a href="#partners" rel="" class="text" key="partners"></a>
      <a href="#newsletter" rel="" class="text" key="newsletter"></a>
      <a href="#contact" rel="" class="text" key="contact"></a>
      <select id="mobile-language">
        <option value="pl">PL</option>
        <option value="en">EN</option>
      </select>
    </div>
    <!-- END OF MOBILE HEADER -->

    <!-- WELCOME SECTION -->
    <div id="start" class="section welcome">
      <div class="welcome-content">
        <div key="mainMessage" class="tedx text"></div>
      </div>
    </div>
    <!-- END OF WELCOME SECTION -->


    <!-- EVENTS SECTION -->
    <div id="eventsSection" class="section events flex-grid">
      <div class="event-item col-2">
        <div class="item-content right-align">
          <div key="firstEventTitle" class="event-title text"></div>
          <div key="firstEventSubtitle" class="event-subtitle text"></div>
          <div key="firstEventDate" class="event-date text"></div>
          <div key="firstEventInfo" class="event-info text"></div>
        </div>
      </div>
      <div class="event-item col-2">
        <div class="item-content left-align">
          <div key="secondEventTitle" class="event-title text"></div>
          <div key="secondEventSubtitle" class="event-subtitle text"></div>
          <div key="secondEventDate" class="event-date text"></div>
          <div key="secondEventInfo" class="event-info text"></div>
        </div>
      </div>
    </div>
    <!-- END OF EVENTS SECTION -->


    <!-- ABOUT US SECTION -->
    <div id="aboutUs" class="section three-items about-us">
      <div key="aboutUs" class="three-items-title text"></div>
      <div class="details">
        <div class="item">
          <div class="left-align three-items-content">
            <div key="aboutUsFirstTitle" class="item-title text"></div>
            <div class="x-mark"></div>
            <div key="aboutUsFirstInfo" class="item-text text"></div>
          </div>
        </div>
        <div class="item">
          <div class="center-align three-items-content">
            <div key="aboutUsSecondTitle" class="item-title text"></div>
            <div class="x-mark"></div>
            <div key="aboutUsSecondInfo" class="item-text text"></div>
          </div>
        </div>
        <div class="item">
          <div class="right-align three-items-content">
            <div key="aboutUsThirdTitle" class="item-title text"></div>
            <div class="x-mark"></div>
            <div key="aboutUsThirdInfo" class="item-text text"></div>
          </div>
        </div>
      </div>
      <div class="question text" key="questionOne"></div>
      <div class="answer">
        <div id="yes" key="yes" class="active text"></div><div id="no" key="no" class="text"></div>
      </div>
    </div>
    <!-- END OF ABOUT US -->

    <!-- POSTS -->
    <div class="section posts">
      <div class="flex-grid post">
        <div class="col-2 post-photo">
        </div>
        <div class="col-2 post-info">
          <div class="item-content left-align">
            <div key="firstEventTitle" class="event-title text"></div>
            <div key="firstEventSubtitle" class="event-subtitle text"></div>
            <div key="firstEventInfo" class="event-info text"></div>
          </div>
        </div>
      </div>
      <div class="flex-grid post reverse-column">
        <div class="col-2 post-info">
          <div class="item-content right-align">
            <div key="firstEventTitle" class="event-title text"></div>
            <div key="firstEventSubtitle" class="event-subtitle text"></div>
            <div key="firstEventInfo" class="event-info text"></div>
          </div>
        </div>
        <div class="col-2 post-photo">
        </div>
      </div>
      <div class="flex-grid post">
        <div class="col-2 post-photo">
        </div>
        <div class="col-2 post-info">
          <div class="item-content left-align">
            <div key="firstEventTitle" class="event-title text"></div>
            <div key="firstEventSubtitle" class="event-subtitle text"></div>
            <div key="firstEventInfo" class="event-info text"></div>
          </div>
        </div>
      </div>
      <div class="flex-grid post reverse-column">
        <div class="col-2 post-info">
          <div class="item-content right-align">
            <div key="firstEventTitle" class="event-title text"></div>
            <div key="firstEventSubtitle" class="event-subtitle text"></div>
            <div key="firstEventInfo" class="event-info text"></div>
          </div>
        </div>
        <div class="col-2 post-photo">
        </div>
      </div>
    </div>
    <!-- END OF POSTS -->

    <!-- PARTNERS -->
    <div id="partners" class="section partners">
      <div class="content flex-grid">
        <div class="questions col-2 right-align item-content">
          <div id="partner-modal" key="partnerQuestionOne" class="question text"></div>
          <div key="partnerQuestionTwo" class="question text"></div>
          <div key="partnerQuestionThree" class="question text"></div>
        </div>
        <div class="email-form col-2 left-align item-content">
          <form id="form">
            <div key="formName" class="email-label text"></div>
            <input id="name" class="email-input" type="text" name="name">
            <div key="formEmail" class="email-label text"></div>
            <input id="email" class="email-input" type="text" name="email">
            <div class="button-container">
              <button type="submit" key="send" class="text"></button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END OF PARTNERS -->

    <!-- PARNTERs MODAL -->
    <div class="partner-modal">
      <div class="backdrop">
      </div>
      <div class="modal">
      </div>
    </div>
    <!-- END OF PARTNERS MODAL -->

    <!-- CONTACT -->

    <div id="contact" class="section three-items">
      <div key="contact" class="three-items-title text"></div>
      <div class="details">
        <div class="item">
          <div class="left-align three-items-content">
            <div key="contactFirstTitle" class="item-title text"></div>
            <div key="contactFirstInfo" class="item-text text"></div>
            <div class="x-mark"></div>
          </div>
        </div>
        <div class="item">
          <div class="center-align three-items-content">
            <div key="contactSecondTitle" class="item-title text"></div>
            <div key="contactSecondInfo" class="item-text text"></div>
            <div class="x-mark"></div>
          </div>
        </div>
        <div class="item">
          <div class="right-align three-items-content">
            <div key="contactThirdTitle" class="item-title text"></div>
            <div key="contactThirdInfo" class="item-text text"></div>
            <div class="x-mark"></div>
          </div>
        </div>
      </div>
    </div>

    <!-- END OF CONTACT -->

    <!-- FOOTER -->

    <div id="footer" class="section footer">
      <div class="footer-links">
        <a href="#eventsSection" rel="" class="text" key="start"></a>
        <a href="#aboutUs" rel="" class="text" key="aboutUs"></a>
        <a href="#partners" rel="" class="text" key="partners"></a>
        <a href="#newsletter" rel="" class="text" key="newsletter"></a>
        <a href="#contact" rel="" class="text" key="contact"></a>
        <a href="#start" ret="" class="button-up" ></a>
      </div>
      <div key="copyright1" class="copyrights text"></div>
      <div key="copyright2" class="copyrights text"></div>
    </div>

    <!-- END OF FOOTER -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    <script src="messages.js" type="text/javascript"></script>
  </body>
</html>
