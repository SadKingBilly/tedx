
// script for scrolling
$('a').click(function(){
    var offset = $('#header').css('position') == 'absolute' ? 0 : 80
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top - offset
    }, 500);
    return false;
});

function sendMail(email, name) {
  $.ajax( { type : 'POST',
    data : {email: email, name: name},
    url  : 'mailSender.php',
  });
}

function showModal() {
  $('.partner-modal').addClass("show-modal")
}

$('#mobile-menu').click(function(){
  if ($('#header').hasClass("fix-header")) $('#header').removeClass("fix-header")
  $('#mobile-header').addClass("show-header")
});

$('.close-menu').click(function(){
  $('#mobile-header').removeClass("show-header")
});

$('.backdrop').click(function(){
  $('.partner-modal').removeClass("show-modal")
});


$('#form').submit(function( event ) {
  event.preventDefault();
  var email = $('#form #email');
  var name = $('#form #name');
  sendMail(email.val(), name.val())
  name.val('')
  email.val('')
});

var doc = $(document)
doc.on("scroll", function(e) {
  if ($('#header').css('position') == 'absolute') return
  if (doc.scrollTop() > 0) {
    $('#header').addClass("fix-header");
  } else {
    $('#header').removeClass("fix-header");
  }
});
