var messages = {
  en: {
    mainMessage: "Hello are you a TEDxer?",
    aboutUs: "About us",
    start: "Start",
    partners: "Partners",
    newsletter: "Newsletter",
    contact: "Contact",

    menu: "Menu",

    firstEventTitle: "Previous event",
    firstEventSubtitle: "TEDxKraków Salon",
    firstEventDate: "15 LUTEGO 2017",
    firstEventInfo: "Lorem ipsum liberatis et cetera et ceteraa description still go on an on§",

    secondEventTitle: "Next event",
    secondEventSubtitle: "TEDxKraków 2017: Deconstruct!",
    secondEventDate: "15 LISTOPADA 2017",
    secondEventInfo: "Alert! Alert! It’s about the time we shake this world up. Deconstruct - learn from the past, rebuild now, to see our societies reach higher tomorrow. Structures rust, systems fall. Isn’t the world awaiting for a drastic change? Which ideas cause goosebumps on you back? What innovation are you waiting for? Is there a way to build up on what was set in stone or…shall we deconstruct?",

    aboutUsFirstTitle: "Eversearching,",
    aboutUsFirstInfo: "Kraków deserves to be on the world innovation map because of its people and its ideas. Our events: big conferences and small meet ups amplify the most thought-provoking ideas and resonate back engaging local community.",

    aboutUsSecondTitle: "well rounded-up,",
    aboutUsSecondInfo: "Almost 10k people got inspired at our events so far. The beat of thrilling ideas and cutting-edge innovation was pounding blatantly. at every event we put together. We’re a group of volunteers, yet still we approach TEDxKraków as professionals. It’s our baby and there’s more to come.",

    aboutUsThirdTitle: "change makers.",
    aboutUsThirdInfo: "We enable local ideas to be heard about, we bring global ideas to our local community. It’s all sizzling. Our events are not only about listening, we empower our participants to bring the change. We’re happy to take you down the road through the TEDxKraków alumni alley.",

    questionOne: "Do you know what is it TED?",
    yes: "yes",
    no: "no",

    partnerQuestionOne: " Do you want to be our <span onclick='showModal()>partner</span>?",
    partnerQuestionTwo: "Or, do you want to join a team?",
    partnerQuestionThree: "Maybe, you want to subscribe ‘Cześć’ from us?",

    formName: "Name",
    formEmail: "Email",
    send: "Send",

    contact: "Contact",

    contactFirstTitle: "GENERAL CONTACT",
    contactFirstInfo: "contact@tedxkrakow.com",
    contactSecondTitle: "Press & Media",
    contactSecondInfo: "media@tedxkrakow.com",
    contactThirdTitle: "PARTNERSHIP",
    contactThirdInfo: "partnership@tedxkrakow.com",

    copyright1: "© TEDxKraków - All rights reserved ",
    copyright2: "If you want to improve this website with us, feel free to contact designer or developer.",
  },
  pl: {
    mainMessage: "Hello are you a TEDxer?",
    aboutUs: "O nas",
    start: "Start",
    partners: "Partnerzy",
    newsletter: "Newsletter",
    contact: "Kontakt",

    menu: "Menu",

    firstEventTitle: "Previous event",
    firstEventSubtitle: "TEDxKraków Salon",
    firstEventDate: "15 LUTEGO 2017",
    firstEventInfo: "Lorem ipsum liberatis et cetera et ceteraa description still go on an on§",

    secondEventTitle: "Next event",
    secondEventSubtitle: "TEDxKraków 2017: Deconstruct!",
    secondEventDate: "15 LISTOPADA 2017",
    secondEventInfo: "Alert! Alert! It’s about the time we shake this world up. Deconstruct - learn from the past, rebuild now, to see our societies reach higher tomorrow. Structures rust, systems fall. Isn’t the world awaiting for a drastic change? Which ideas cause goosebumps on you back? What innovation are you waiting for? Is there a way to build up on what was set in stone or…shall we deconstruct?",

    aboutUsFirstTitle: "Eversearching,",
    aboutUsFirstInfo: "Kraków deserves to be on the world innovation map because of its people and its ideas. Our events: big conferences and small meet ups amplify the most thought-provoking ideas and resonate back engaging local community.",

    aboutUsSecondTitle: "well rounded-up,",
    aboutUsSecondInfo: "Almost 10k people got inspired at our events so far. The beat of thrilling ideas and cutting-edge innovation was pounding blatantly. at every event we put together. We’re a group of volunteers, yet still we approach TEDxKraków as professionals. It’s our baby and there’s more to come.",

    aboutUsThirdTitle: "change makers.",
    aboutUsThirdInfo: "We enable local ideas to be heard about, we bring global ideas to our local community. It’s all sizzling. Our events are not only about listening, we empower our participants to bring the change. We’re happy to take you down the road through the TEDxKraków alumni alley.",

    questionOne: "Do you know what is it TED?",
    yes: "yes",
    no: "no",

    partnerQuestionOne: " Do you want to be our <span onclick='showModal()'>partner</span>?",
    partnerQuestionTwo: "Or, do you want to join a team?",
    partnerQuestionThree: "Maybe, you want to subscribe ‘Cześć’ from us?",

    formName: "Name",
    formEmail: "Email",
    send: "Send",

    contact: "Contact",

    contactFirstTitle: "GENERAL CONTACT",
    contactFirstInfo: "contact@tedxkrakow.com",
    contactSecondTitle: "Press & Media",
    contactSecondInfo: "media@tedxkrakow.com",
    contactThirdTitle: "PARTNERSHIP",
    contactThirdInfo: "partnership@tedxkrakow.com",

    copyright1: "© TEDxKraków - All rights reserved ",
    copyright2: "If you want to improve this website with us, feel free to contact designer or developer.",
  }
}

function changeLng(lng) {
  $(".text").each(function(){
    $(this).html(messages[lng][$(this).attr("key")])
  })
}

changeLng('pl')

$('#language, #mobile-language').change(function(event) {
  $('#language, #mobile-language').val(event.target.value)
  changeLng(event.target.value)
});
